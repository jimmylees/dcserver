package org.zgjy;

import java.util.ArrayList;
import lombok.*;
import org.zgjy.model.Field;
import org.zgjy.model.MachineType;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DeviceConfig {

    public MachineType machineType;
    public ArrayList<Field> fields;
}

