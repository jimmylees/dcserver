package org.zgjy.test;



import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.zgjy.mapper.ModelMapper;
import org.zgjy.model.MachineType;

import java.io.InputStream;

public class SqliteTest {

    public static void main(String[] args) throws Exception {

        //String resource = "F://DCServer//src//main//resources//mybatis_conf.xml";
        String resource = "mybatis_conf.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        try(SqlSession session = sqlSessionFactory.openSession()){
            ModelMapper mapper = session.getMapper(ModelMapper.class);
            MachineType mtype = new MachineType(1l, "xxx", "pId_001", "xxx");
            mapper.insertMachineType(mtype);
            session.commit();
        }
    }
}
