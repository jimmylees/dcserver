package org.zgjy.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
//@MapperScan(basePackages = {"org"})
public class SpingBootTest {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(SpingBootTest.class, args);
    }
}
