package org.zgjy.test;

import com.alibaba.fastjson.JSON;
import org.zgjy.DeviceConfig;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


public class JsonTest {

    public static void main(String[] args) {

        String filename = "src/main/resources/config/CL_FA201B.json";
        Path path = Paths.get(filename);
        String content;

        try{

            content = Files.readString(path);
            System.out.println("### config.json content ###");
            System.out.println(content);
            System.out.println("### config.json content ###");

            DeviceConfig config = JSON.parseObject(content, DeviceConfig.class);
            System.out.println(config.toString());

        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }
}
