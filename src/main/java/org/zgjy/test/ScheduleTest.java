package org.zgjy.test;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.zgjy.ModbusTimerTask;
import org.zgjy.mapper.ModelMapper;
import org.zgjy.model.Device;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class ScheduleTest {

    public static void main(String[] args) throws Exception {

//        ScheduledExecutorService schedService = new ScheduledThreadPoolExecutor(5);
//        Runnable r = ()-> System.out.println("Thread:" + Thread.currentThread().getName() + ", Time: " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss")));
//
//        Runnable modbus_run = new Runnable() {
//            @Override
//            public void run() {
//
//            }
//        };
//
//        schedService.scheduleAtFixedRate(r, 3, 5, TimeUnit.SECONDS);
//
//        Thread.sleep(1000 * 50);
//        schedService.shutdown();
//        System.out.println("### program end ###");
        ArrayList<String> deviceGoup1 = new ArrayList<>();
        List<Device> devices;

        String resource = "mybatis_conf.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        try(SqlSession session = sqlSessionFactory.openSession()){
            ModelMapper mapper = session.getMapper(ModelMapper.class);
            devices = mapper.selectDevice();
        }

        TimerTask timerTask = new ModbusTimerTask("Task1", devices);
        //TimerTask timerTask2 = new ModbusTimerTask("Task2", deviceGoup2);

        Timer timer1 = new Timer();
        timer1.schedule(timerTask, 3L, 3000L);

        //Timer timer2 = new Timer();
        //timer2.schedule(timerTask2, 3L, 1000L);

        String strTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss"));
        System.out.println("主线程:" + Thread.currentThread().getId() + " Time:" + strTime);

    }
}
