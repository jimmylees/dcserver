package org.zgjy.model;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.serotonin.modbus4j.ModbusFactory;
import com.serotonin.modbus4j.ModbusMaster;
import com.serotonin.modbus4j.exception.ModbusTransportException;
import com.serotonin.modbus4j.ip.IpParameters;
import com.serotonin.modbus4j.msg.ModbusRequest;
import com.serotonin.modbus4j.msg.ModbusResponse;
import lombok.*;

@Data
@AllArgsConstructor
@TableName("device")
public class Device {

    @TableId(value="id", type= IdType.AUTO)
    public Long id;

    @TableField(value="productId")
    public String productId;

    @TableField(value="macNo")
    public String macNo;

    @TableField(value="ip")
    public String ip;

    @TableField(value="port")
    public int port;

}
