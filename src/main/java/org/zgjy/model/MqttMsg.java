package org.zgjy.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

@Data
@TableName("mqtt_msg")
public class MqttMsg {

    @TableId(value="id", type= IdType.AUTO)
    public Long id;
    public String macId;
    public String macNo;
    public long ts;
    public String content;
    public String sendTime;

    public MqttMsg(String macId, String macNo, long ts, String content, String sendTime) {
        this.macId = macId;
        this.macNo = macNo;
        this.ts = ts;
        this.content = content;
        this.sendTime = sendTime;
    }
}
