package org.zgjy.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("field")
public class Field {
    @TableField(value="port_id")
    public int port_id;

    @TableField(value="addr")
    public int addr;

    @TableField(value="mqtt_tag")
    public String mqtt_tag;

    @TableField(value="data_type")
    public String data_type;

    @TableField(value="endian")
    public String endian;

    @TableField(value="desc")
    public String desc;

    @TableField(value="coefficient")
    public float coefficient;
}
