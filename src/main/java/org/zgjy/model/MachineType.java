package org.zgjy.model;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;


@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("machine_type")
public class MachineType {

    @TableId(value="id", type= IdType.AUTO)
    public Long id;

    @TableField(value="TOPIC")
    public String TOPIC;

    @TableField(value="productId")
    public String productId;

    @TableField(value="desc")
    public String desc;
}
