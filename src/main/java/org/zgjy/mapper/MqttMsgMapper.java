package org.zgjy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.transaction.annotation.Transactional;
import org.zgjy.model.MqttMsg;

@Mapper
@Transactional
public interface MqttMsgMapper extends BaseMapper<MqttMsg> {
}
