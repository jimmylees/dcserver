package org.zgjy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.zgjy.model.Device;

@Mapper
public interface ClassMapper extends BaseMapper<Device> {
}
