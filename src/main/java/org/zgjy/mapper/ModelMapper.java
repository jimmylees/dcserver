package org.zgjy.mapper;
import org.zgjy.model.Device;
import org.zgjy.model.Field;
import org.zgjy.model.MachineType;

import java.util.List;


public interface ModelMapper {

    public Field selectFieldById(int id);
    public Device selectDeviceById(int id);
    public MachineType selectMachineTypeById(int id);

    public List<Field> selectField();
    public List<Device> selectDevice();
    public List<MachineType> selectMachineType();

    public Integer insertMachineType(MachineType mtype);

}
