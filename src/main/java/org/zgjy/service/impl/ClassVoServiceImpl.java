package org.zgjy.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Component;
import org.zgjy.mapper.ClassMapper;
import org.zgjy.model.Device;
import org.zgjy.service.ClassVoService;

@Component
public class ClassVoServiceImpl extends ServiceImpl<ClassMapper, Device> implements ClassVoService {
    @Override
    public String getDeviceName(Long id) {

        Device device = getById(id);
        if(device != null)
            return device.toString();
        else
            return null;
    }
}
