package org.zgjy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.zgjy.model.Device;

public interface ClassVoService extends IService<Device> {

    String getDeviceName(Long id);
}
