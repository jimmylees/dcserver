package org.zgjy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.zgjy.service.ClassVoService;


@RestController
@RequestMapping("/dcs")
public class MainControler {

    @Autowired
    ClassVoService classVoService;

    @RequestMapping("/test")
    public String test(){
        return "Success";
    }

    @GetMapping("/getDeviceById")
    public String getDeviceById(Long id){

        System.out.println("/getDeviceById?id=" + id);
        String deviceName = classVoService.getDeviceName(id);
        if(deviceName != null){
            return deviceName;
        }

        return "No Record";
    }
}
