package org.zgjy;


import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

//@Component
//@EnableScheduling
public class CronTaskTest {

    //@Scheduled(cron="*/5 * * * * ?")
    public void run(){

        Thread currentThread = Thread.currentThread();
        System.out.println("Hello: " + new Date().toString() + " - ThreadID: " + currentThread.getId() + " , ThreadName: " + currentThread.getName());
    }
}
